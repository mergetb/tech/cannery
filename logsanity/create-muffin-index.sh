#!/bin/bash

ct="Content-Type: application/json"
rq='{ "settings" : { "number_of_shards": 3, "number_of_replicas": 2 } }'

curl -H "$ct" -d "$rq" -X PUT 10.0.0.100:9200/muffins | jq
